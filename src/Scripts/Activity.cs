public enum PlayerActivity {
    Status,
    Food,
    Play,
    Medicine,
    Clean,
    Bathroom,
    Sleep,
    ClickOn
}