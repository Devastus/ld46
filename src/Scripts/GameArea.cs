using Godot;
using System;
using Utils;

[Tool]
public class GameArea : Node2D
{
    [Export]
    public Vector2 areaRectSize;
    [Export]
    private Color debugColor = new Color(1f, 0, 0, 1f);
    [Export]
    private bool itemActive = false;

    private Vector2 itemPos;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        if (!Godot.Engine.EditorHint) GameManager.OnUpdateDebug += () => Update();
        if (Godot.Engine.EditorHint || GameManager.Instance.Debug) Update();
    }

    public override void _Draw()
    {
        if (Godot.Engine.EditorHint || GameManager.Instance.Debug)
        {
            DrawRect(new Rect2(new Vector2(0f, 0f), areaRectSize), debugColor, false, 3f);
            if (itemActive) {
                DrawCircle(itemPos, 10f, new Color(0f, 0f, 1f, 1f));
            }
        }
    }

    //  // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(float delta)
    {
        if (Godot.Engine.EditorHint || GameManager.Instance.Debug) Update();
        if (itemActive) {
            itemPos = GetGameAreaMousePos();
        }
    }

    // public override void _UnhandledInput(InputEvent @event) {
    //     if (itemActive && @event is InputEventMouseButton) {
    //         GD.Print("BABABA");
    //     }
    // }

    public Vector2 ClampPositionToRect(Vector2 position, Vector2 margin = default(Vector2))
    {
        Rect2 rect = GetRect(margin);
        return MathUtils.ClampRect(position, rect);
    }

    public bool HasPosition(Vector2 position, Vector2 margin = default(Vector2))
    {
        return GetRect(margin).HasPoint(position);
    }

    public Rect2 GetRect(Vector2 margin = default(Vector2)) {
        return new Rect2(
            new Vector2(Position.x + margin.x, Position.y + margin.y),
            new Vector2(areaRectSize.x - margin.x, areaRectSize.y - margin.y)
        );
    }


    public Vector2 GetGameAreaMousePos() {
        return GetGlobalMousePosition().ClampRect(GetRect()) - Position;
    }
}
