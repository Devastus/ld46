using Godot;
using System;

public class GameManager : Node
{
    private static GameManager _instance;
    public static GameManager Instance { get { return _instance; } }

    public static System.Action OnUpdateDebug;

    [Export]
    private PackedScene sceneFactoryScene;
    [Export]
    private bool debug;
    public bool Debug { get { return debug; } }

    public TickManager tickManager = new TickManager();
    public PointsManager pointsManager = new PointsManager();

    private Node2D _sceneRoot;
    private Node2D _sceneFactory;
    private Node2D _curScene;
    private Entity _curEntity;

    public Node2D CurScene { get { return _curScene; } }
    public Entity CurEntity { get { return _curEntity; } }

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        _instance = this;
        AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionHandler;
        _sceneFactory = (Node2D)sceneFactoryScene.Instance();
        _sceneRoot = GetNode<Node2D>("SceneRoot");
        tickManager.Start();
        UIManager.OnSetScreen(UIManager.Screen.Start);
    }

    //  // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(float delta)
    {
        tickManager.Update(delta);
    }

    public override void _Input(InputEvent @event) {
        if (!OS.HasFeature("release")) {
            if (@event is InputEventKey) {
                var key = (InputEventKey)@event;
                if (key.Pressed && key.Scancode == (uint)KeyList.F12) {
                    debug = !debug;
                    if (OnUpdateDebug != null) OnUpdateDebug();
                }
            }
        }
    }

    public void StartGame() {
        _curScene = SpawnNode<Node2D>("GameScene");
        _curEntity = (Entity)_curScene.GetNode("Entity");
    }

    public T SpawnNode<T>(int nodeIndex, Node parent = null) where T : Node
    {
        T node = (T)_sceneFactory.GetChild(nodeIndex).Duplicate();
        if (parent != null)
        {
            parent.AddChild(node);
        }
        else
        {
            _sceneRoot.AddChild(node);
        }
        return node;
    }

    public T SpawnNode<T>(string nodeName, Node parent = null) where T : Node
    {
        T node = (T)_sceneFactory.GetNode(nodeName).Duplicate();
        if (parent != null)
        {
            parent.AddChild(node);
        }
        else
        {
            _sceneRoot.AddChild(node);
        }
        return node;
    }

    public void DespawnNode(Node node, Node parent = null)
    {
        if (parent != null)
        {
            parent.RemoveChild(node);
        }
        else
        {
            _sceneRoot.RemoveChild(node);
        }
        node.QueueFree();
    }

    private void UnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs e)
    {
        GD.PrintS($"Got unhandled exception {e.ExceptionObject}");
        GD.PrintErr("Quitting due to exception");
    }
}
