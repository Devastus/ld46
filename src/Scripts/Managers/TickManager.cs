using Godot;
using System;

public class TickManager
{
    public const float TICK_DURATION = 1f;
    public const uint DAY_DURATION = 30;
    public static System.Action OnTickUpdate;
    public static System.Action<ulong> OnDayUpdate;

    private float _timePerTick;
    private uint _ticksPerDay;
    private ulong _days;
    public ulong Days { get {return _days;}}

    public void Start()
    {
        _timePerTick = 0;
    }

    public void Update(float delta)
    {
        _timePerTick += delta;
        if (_timePerTick >= TICK_DURATION)
        {
            if (OnTickUpdate != null)
            {
                OnTickUpdate();
            }
            _ticksPerDay += 1;
            _timePerTick -= TICK_DURATION;
        }

        if (_ticksPerDay >= DAY_DURATION) {
            _days += 1;
            _ticksPerDay = 0;
            if (OnDayUpdate != null)
            {
                OnDayUpdate(_days);
            }
        }
    }
}