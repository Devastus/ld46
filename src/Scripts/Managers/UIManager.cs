using Godot;
using System;
using static Status;

public class UIManager : CanvasLayer
{
    public enum Screen {
        Start,
        Game,
        End
    }

    public static System.Action<PlayerActivity> OnPressActivity;
    public static System.Action<Stats> OnEntityStatsUpdate;
    public static System.Action<Screen> OnSetScreen;

    private Control _startScreen;
    private Control _gameScreen;
    private Control _endScreen;

    private Control _game_statusNoteNode;
    private Label _game_pointsLabel;
    private Label _game_daysLabel;
    private Label _game_statusLabel;

    private Label _end_loveLabel;
    private Label _end_daysLabel;

    public override void _Ready() {
        TickManager.OnDayUpdate += ChangeDay;
        PointsManager.OnGetPoints += ChangePoints;
        OnEntityStatsUpdate += ChangeStatus;
        OnSetScreen += SetScreen;

        _startScreen = (Control)GetNode("StartScreen");
        _gameScreen = (Control)GetNode("GameScreen");
        _endScreen = (Control)GetNode("EndScreen");

        _game_statusNoteNode = (Control)_gameScreen.GetNode("StatusNote");
        _game_statusLabel = (Label)_gameScreen.GetNode("StatusNote/StatusNoteTex/StatusLabel");
        _game_pointsLabel = (Label)_gameScreen.GetNode("Points/PointsLabel");
        _game_daysLabel = (Label)_gameScreen.GetNode("Days/DaysLabel");
        
        _end_loveLabel = (Label)_endScreen.GetNode("Container/LoveContainer/LoveLabelNumber");
        _end_daysLabel = (Label)_endScreen.GetNode("Container/DaysLabel");
    }

    public void _on_StartGameButton_pressed()
    {
        GameManager.Instance.StartGame();
        OnSetScreen(Screen.Game);
    }

    public void _on_StatusButton_pressed()
    {
        if (OnPressActivity != null)
        {
            _game_statusNoteNode.Visible = !_game_statusNoteNode.Visible;
            OnPressActivity(PlayerActivity.Status);
        }
    }

    public void _on_FoodButton_pressed()
    {
        if (OnPressActivity != null)
        {
            OnPressActivity(PlayerActivity.Food);
        }
    }

    public void _on_PlayButton_pressed()
    {
        if (OnPressActivity != null)
        {
            OnPressActivity(PlayerActivity.Play);
        }
    }

    public void _on_MedicineButton_pressed()
    {
        if (OnPressActivity != null)
        {
            OnPressActivity(PlayerActivity.Medicine);
        }
    }

    public void _on_CleanButton_pressed()
    {
        if (OnPressActivity != null)
        {
            OnPressActivity(PlayerActivity.Clean);
        }
    }

    public void _on_BathroomButton_pressed()
    {
        if (OnPressActivity != null)
        {
            OnPressActivity(PlayerActivity.Bathroom);
        }
    }

    public void _on_SleepButton_pressed()
    {
        if (OnPressActivity != null)
        {
            OnPressActivity(PlayerActivity.Sleep);
        }
    }

    public void ChangeDay(ulong days) {
        _game_daysLabel.Text = $"{days.ToString()} days";
    }

    public void ChangePoints(ulong points) {
        _game_pointsLabel.Text = points.ToString();
    }

    public void ChangeStatus(Stats entityStats) 
    {
        for (int i = 0; i < statusPriorities.Length; i++) {
            var status = entityStats.GetStatus(statusPriorities[i].statType);
            if (status == Status.StatusName.Critical)
            {
                _game_statusLabel.Text = $"Doesn't look good...";
                return;
            }
            else if (status == statusPriorities[i].status)
            {
                _game_statusLabel.Text = $"Looks {statusPriorities[i].status.ToString().ToLower()}!";
                return;
            }
        }
        _game_statusLabel.Text = "Looks confusing!";
    }

    public void SetScreen(Screen screen) {
        switch (screen) {
            case Screen.Start:
                _startScreen.Visible = true;
                _gameScreen.Visible = false;
                _endScreen.Visible = false;
                break;
            case Screen.Game:
                _startScreen.Visible = false;
                _gameScreen.Visible = true;
                _endScreen.Visible = false;
                break;
            case Screen.End:
                _end_loveLabel.Text = GameManager.Instance.pointsManager.Points.ToString();
                _end_daysLabel.Text = $"And lived for {GameManager.Instance.tickManager.Days.ToString()} days";
                _startScreen.Visible = false;
                _gameScreen.Visible = false;
                _endScreen.Visible = true;
                break;
            default:
                break;
        }
    }
}