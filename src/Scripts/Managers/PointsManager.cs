using Godot;
using System;

public class PointsManager {
    public static System.Action<ulong> OnGetPoints;
    private ulong _points = 0;
    public ulong Points { get { return _points; } }

    private float _pointsTimer = 0f;

    public void Start()
    {
        _points = 0;
    }

    public void UpdatePoints(Entity entity)
    {
        if (entity != null && 
            entity.CurInteractionState != Entity.InteractionState.Dead
        ) {
            Stats stats = GameManager.Instance.CurEntity.CurStats;
            if (
                stats.GetStatus(Stats.Type.Health) == Status.StatusName.Healthy &&
                stats.GetStatus(Stats.Type.Fullness) == Status.StatusName.Full &&
                stats.GetStatus(Stats.Type.Happiness) == Status.StatusName.Happy &&
                stats.GetStatus(Stats.Type.Cleanliness) == Status.StatusName.Clean &&
                stats.GetStatus(Stats.Type.Energy) == Status.StatusName.Energetic
            ) {
                AddPoints();
            }
        }
    }

    public void AddPoints() {
        _pointsTimer += 0.5f;
        if (_pointsTimer >= 1f) {
            _points += 1;
            _pointsTimer = 0f;
            if (OnGetPoints != null) OnGetPoints(_points);
        }
    }
}