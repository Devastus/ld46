using Godot;
using System;
using static Utils.MathUtils;

public class GameCamera : Camera2D
{
    public enum Mode {
        Default,
        Activity,
        Reaction
    }

    public static System.Action<Node2D> OnSetTarget;
    public static System.Action<Mode, float> OnSetMode;
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";
    [Export]
    private Vector2 offset = Vector2.Zero;
    [Export]
    private float cameraSpeed = 20f;
    [Export]
    private float zoomSpeed = 0.5f;
    [Export]
    private float minZoom = 0.7f;
    [Export]
    private float maxZoom = 1f;
    [Export]
    private float minZoomTimer = 4f;
    [Export]
    private float maxZoomTimer = 20f;
    [Export]
    private float activityZoom = 0.5f;
    [Export]
    private float reactionZoom = 0.6f;
    [Export]
    private float reactionZoomSine = 0.2f;
    [Export]
    private float wiggleFactor = 1f;
    [Export]
    private float wiggleSpeed = 4f;
    // [Export]
    // private float maxZoomTimer = 20f;
    
    private Mode _mode;
    private Node2D _target;
    private float _zoomTimer = 10f;
    private float _curZoom;
    private float _curWiggleMult = 0f;
    private float _curWiggleTimer = 0f;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        OnSetMode = SetMode;
        OnSetTarget = SetTarget;
        _target = null;
        _zoomTimer = 10f;
        _curZoom = Zoom.x;
    }

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(float delta)
    {
        if (_target != null) {
            Vector2 wiggleVector = GetWiggleVector(delta);
            Position = Position.Lerp(_target.Position + offset, delta * cameraSpeed) + wiggleVector;
            Zoom = Zoom.Lerp(new Vector2(_curZoom, _curZoom), delta * zoomSpeed);
            if (_mode == Mode.Default) {
                _zoomTimer -= delta;
                if (_zoomTimer <= 0f) {
                    RandomizeZoom();
                }
            }

        }
    }

    public void SetTarget(Node2D target) {
        _target = target;
    }

    public void SetMode(Mode mode, float wiggle = 0f) {
        _mode = mode;
        _curWiggleMult = wiggle;
        switch (_mode) {
            case Mode.Activity:
                _curZoom = activityZoom;
                break;
            case Mode.Reaction:
                _curZoom = reactionZoom;
                break;
            default:
                RandomizeZoom();
                break;
        }
    }

    private void RandomizeZoom() {
        _curZoom = (float)GD.RandRange(minZoom, maxZoom);
        _zoomTimer = (float)GD.RandRange(minZoomTimer, maxZoomTimer);
    }

    private Vector2 GetWiggleVector(float delta) {
        _curWiggleTimer += delta;
        return new Vector2(
            (Mathf.Sin(_curWiggleTimer * wiggleSpeed) * wiggleFactor) % 2f,
            (Mathf.Sin(_curWiggleTimer * (wiggleSpeed * 0.75f)) * wiggleFactor) % 2f
        ) * _curWiggleMult;
    }
}
