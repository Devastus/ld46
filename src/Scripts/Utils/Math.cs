using Godot;
using System;

namespace Utils
{

    public static class MathUtils
    {

        public static int Clamp(int value, int min, int max)
        {
            return System.Math.Min(max, System.Math.Max(value, min));
        }

        public static float Clamp(float value, float min, float max)
        {
            return Mathf.Min(max, Mathf.Max(value, min));
        }

        public static Vector2 Lerp(this Vector2 a, Vector2 b, float t) {
            return new Vector2(Mathf.Lerp(a.x, b.x, t), Mathf.Lerp(a.y, b.y, t));
        }

        public static Vector2 ClampRect(this Vector2 v, Rect2 rect) {
            return new Vector2(
                Clamp(v.x, rect.Position.x, rect.Position.x + rect.Size.x),
                Clamp(v.y, rect.Position.y, rect.Position.y + rect.Size.y)
            );
        }
    }

}