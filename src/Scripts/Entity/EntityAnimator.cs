using Godot;
using System;

public class EntityAnimator {

    public enum AnimationSpriteType
    {
        Face,
        Body,
        Dirt,
        Tail,
        Effect,
        Item,
        Delighted,
        Sleep,
        Death
    }

    public enum AnimatorType {
        Default,
        Effect
    }

    public enum AnimationType {
        Idle,
        Walk,
        Delighted,
        Angry,
        Sleeping,
        Dead
    }

    public enum ReactionType {
        None,
        Delighted,
        Neutral,
        Angry
    }

    public enum ActivityType {
        None,
        Eat,
        Play,
        Medicine,
        Clean
    }

    public const string NONE_ANIM = "none";
    public const string NEUTRAL_ANIM = "neutral";
    public const string DELIGHTED_ANIM = "delighted";
    public const string ANGRY_ANIM = "angry";
    public const string HAPPY_ANIM = "happy";
    public const string HUNGRY_ANIM = "hungry";
    public const string SAD_ANIM = "sad";
    public const string SICK_ANIM = "sick";
    public const string OVEREAT_ANIM = "overeat";
    public const string OVERMED_ANIM = "overmed";
    public const string DIRTY_ANIM = "dirty";
    public const string SLEEP_ANIM = "sleep";
    public const string DEAD_ANIM = "dead";
    public const string WALK_ANIM = "walk";
    public const string PLAY_ANIM = "play";
    public const string EAT_ANIM = "eat";
    public const string MEDICINE_ANIM = "medicine";
    public const string MESSY_ANIM = "messy";
    public const string TIRED_ANIM = "tired";
    public const string CLEAN_ANIM = "clean";

    private AnimatedSprite tailAnimSprites;
    private AnimatedSprite bodyAnimSprites;
    private AnimatedSprite dirtAnimSprites;
    private AnimatedSprite faceAnimSprites;
    private AnimatedSprite itemAnimSprites;
    private AnimatedSprite effectAnimSprites;

    private AnimationPlayer animPlayer;
    private AnimationPlayer effectAnimPlayer;

    private Sprite sleepingSprite;
    private Sprite deadSprite;
    private Sprite frontLeftLeg;
    private Sprite frontRightLeg;
    private Sprite backLeftLeg;
    private Sprite backRightLeg;

    // private ReactionType _savedActivityReaction;

    // ANIMATION STATE PRIORITIES
    private EntityAnimStatePriorityValue[] faceAnimPrios = {
        new EntityAnimStatePriorityValue(Stats.Type.Health, Status.StatusName.Critical, SICK_ANIM),
        new EntityAnimStatePriorityValue(Stats.Type.Fullness, Status.StatusName.Critical, SICK_ANIM),
        new EntityAnimStatePriorityValue(Stats.Type.Health, Status.StatusName.Sick, SICK_ANIM),
        new EntityAnimStatePriorityValue(Stats.Type.Health, Status.StatusName.Poisoned, OVERMED_ANIM),
        new EntityAnimStatePriorityValue(Stats.Type.Fullness, Status.StatusName.Stuffed, OVEREAT_ANIM),
        new EntityAnimStatePriorityValue(Stats.Type.Fullness, Status.StatusName.Hungry, HUNGRY_ANIM),
        new EntityAnimStatePriorityValue(Stats.Type.Energy, Status.StatusName.Tired, TIRED_ANIM),
        new EntityAnimStatePriorityValue(Stats.Type.Happiness, Status.StatusName.Sad, SAD_ANIM),
        new EntityAnimStatePriorityValue(Stats.Type.Happiness, Status.StatusName.Neutral, NEUTRAL_ANIM),
        new EntityAnimStatePriorityValue(Stats.Type.Happiness, Status.StatusName.Happy, HAPPY_ANIM)
    };

    private EntityAnimStatePriorityValue[] tailAnimPrios = {
        new EntityAnimStatePriorityValue(Stats.Type.Health, Status.StatusName.Critical, SAD_ANIM),
        new EntityAnimStatePriorityValue(Stats.Type.Fullness, Status.StatusName.Critical, SAD_ANIM),
        new EntityAnimStatePriorityValue(Stats.Type.Health, Status.StatusName.Sick, SAD_ANIM),
        new EntityAnimStatePriorityValue(Stats.Type.Happiness, Status.StatusName.Sad, SAD_ANIM),
        new EntityAnimStatePriorityValue(Stats.Type.Happiness, Status.StatusName.Neutral, NEUTRAL_ANIM),
        new EntityAnimStatePriorityValue(Stats.Type.Happiness, Status.StatusName.Happy, NEUTRAL_ANIM)
    };

    private EntityAnimStatePriorityValue[] bodyAnimPrios = {
        new EntityAnimStatePriorityValue(Stats.Type.Health, Status.StatusName.Critical, SAD_ANIM),
        new EntityAnimStatePriorityValue(Stats.Type.Fullness, Status.StatusName.Critical, SAD_ANIM),
        new EntityAnimStatePriorityValue(Stats.Type.Health, Status.StatusName.Sick, SAD_ANIM),
        new EntityAnimStatePriorityValue(Stats.Type.Fullness, Status.StatusName.Hungry, SAD_ANIM),
        new EntityAnimStatePriorityValue(Stats.Type.Happiness, Status.StatusName.Sad, SAD_ANIM),
        new EntityAnimStatePriorityValue(Stats.Type.Happiness, Status.StatusName.Neutral, NEUTRAL_ANIM),
        new EntityAnimStatePriorityValue(Stats.Type.Happiness, Status.StatusName.Happy, NEUTRAL_ANIM)
    };

    private EntityAnimStatePriorityValue[] dirtAnimPrios = {
        new EntityAnimStatePriorityValue(Stats.Type.Cleanliness, Status.StatusName.Messy, MESSY_ANIM),
        new EntityAnimStatePriorityValue(Stats.Type.Cleanliness, Status.StatusName.Clean, NEUTRAL_ANIM)
    };

    public void Init(Entity entity) {
        tailAnimSprites = entity.GetNode<AnimatedSprite>("SpriteParts/TailSprites");
        bodyAnimSprites = entity.GetNode<AnimatedSprite>("SpriteParts/BodySprites");
        dirtAnimSprites = entity.GetNode<AnimatedSprite>("SpriteParts/BodySprites/DirtSprites");
        faceAnimSprites = entity.GetNode<AnimatedSprite>("SpriteParts/FaceSprites");
        itemAnimSprites = entity.GetNode<AnimatedSprite>("SpriteParts/ItemSprites");
        effectAnimSprites = entity.GetNode<AnimatedSprite>("EffectSprites");
        sleepingSprite = entity.GetNode<Sprite>("SleepingSprite");
        deadSprite = entity.GetNode<Sprite>("DeadSprite");

        frontLeftLeg = entity.GetNode<Sprite>("SpriteParts/FrontLeftLeg");
        frontRightLeg = entity.GetNode<Sprite>("SpriteParts/FrontRightLeg");
        backLeftLeg = entity.GetNode<Sprite>("SpriteParts/BackLeftLeg");
        backRightLeg = entity.GetNode<Sprite>("SpriteParts/BackRightLeg");

        animPlayer = entity.GetNode<AnimationPlayer>("AnimationPlayer");
        effectAnimPlayer = entity.GetNode<AnimationPlayer>("EffectSprites/AnimationPlayer");

        tailAnimSprites.Animation = NEUTRAL_ANIM;
        tailAnimSprites.Playing = true;
        bodyAnimSprites.Animation = NEUTRAL_ANIM;
        bodyAnimSprites.Playing = true;
        dirtAnimSprites.Animation = NEUTRAL_ANIM;
        dirtAnimSprites.Playing = true;
        faceAnimSprites.Animation = NEUTRAL_ANIM;
        faceAnimSprites.Playing = true;
        itemAnimSprites.Animation = NEUTRAL_ANIM;
        itemAnimSprites.Playing = true;
        effectAnimSprites.Animation = NEUTRAL_ANIM;
        effectAnimSprites.Playing = true;

        animPlayer.Play(NEUTRAL_ANIM);
        effectAnimPlayer.Play(NEUTRAL_ANIM);
    }

    public void SetAnimationFrames(
        string faceAnimKey = NEUTRAL_ANIM,
        string bodyAnimKey = NEUTRAL_ANIM,
        string tailAnimKey = NEUTRAL_ANIM,
        string effectAnimKey = NEUTRAL_ANIM
    )
    {
        faceAnimSprites.Animation = faceAnimKey;
        bodyAnimSprites.Animation = bodyAnimKey;
        tailAnimSprites.Animation = tailAnimKey;
        effectAnimSprites.Animation = effectAnimKey;
    }

    public void SetAnimationFrameByPrio(Stats stats, AnimationSpriteType spriteType)
    {
        SetAnimationFrame(spriteType, GetAnimKeyFromPrio(stats, spriteType));
    }

    public void SetAnimationFrame(AnimationSpriteType spriteType, string animationKey) {
        switch (spriteType)
        {
            case AnimationSpriteType.Face:
                faceAnimSprites.Animation = animationKey;
                break;
            case AnimationSpriteType.Body:
                bodyAnimSprites.Animation = animationKey;
                break;
            case AnimationSpriteType.Dirt:
                dirtAnimSprites.Animation = animationKey;
                break;
            case AnimationSpriteType.Tail:
                tailAnimSprites.Animation = animationKey;
                break;
            case AnimationSpriteType.Effect:
                effectAnimSprites.Animation = animationKey;
                break;
            case AnimationSpriteType.Item:
                itemAnimSprites.Animation = animationKey;
                break;
            default:
                break;
        }
    }

    public void SetSpriteVisibility(AnimationType animationType)
    {
        switch (animationType)
        {
            case AnimationType.Sleeping:
                faceAnimSprites.Visible = false;
                bodyAnimSprites.Visible = false;
                tailAnimSprites.Visible = false;
                effectAnimSprites.Visible = true;
                dirtAnimSprites.Visible = false;
                itemAnimSprites.Visible = false;
                sleepingSprite.Visible = true;
                deadSprite.Visible = false;

                frontLeftLeg.Visible = false;
                frontRightLeg.Visible = false;
                backLeftLeg.Visible = false;
                backRightLeg.Visible = false;
                break;
            case AnimationType.Dead:
                faceAnimSprites.Visible = false;
                bodyAnimSprites.Visible = false;
                tailAnimSprites.Visible = false;
                effectAnimSprites.Visible = true;
                dirtAnimSprites.Visible = false;
                itemAnimSprites.Visible = false;
                sleepingSprite.Visible = false;
                deadSprite.Visible = true;

                frontLeftLeg.Visible = false;
                frontRightLeg.Visible = false;
                backLeftLeg.Visible = false;
                backRightLeg.Visible = false;
                break;
            default:
                faceAnimSprites.Visible = true;
                bodyAnimSprites.Visible = true;
                tailAnimSprites.Visible = true;
                effectAnimSprites.Visible = true;
                dirtAnimSprites.Visible = true;
                itemAnimSprites.Visible = false;
                sleepingSprite.Visible = false;
                deadSprite.Visible = false;

                frontLeftLeg.Visible = true;
                frontRightLeg.Visible = true;
                backLeftLeg.Visible = true;
                backRightLeg.Visible = true;
                break;
        }
    }

    public void PlayActivity(ActivityType activity) {
        switch(activity) {
            case ActivityType.Play:
                frontLeftLeg.Visible = false;
                frontRightLeg.Visible = false;
                backLeftLeg.Visible = false;
                backRightLeg.Visible = false;
                dirtAnimSprites.Visible = false;
                itemAnimSprites.Visible = true;
                SetAnimationFrame(AnimationSpriteType.Face, NONE_ANIM);
                SetAnimationFrame(AnimationSpriteType.Item, NEUTRAL_ANIM);
                SetAnimationFrame(AnimationSpriteType.Body, PLAY_ANIM);
                SetAnimationFrame(AnimationSpriteType.Tail, PLAY_ANIM);
                PlayAnimation(PLAY_ANIM);
                break;
            case ActivityType.Eat:
                // frontLeftLeg.Visible = false;
                // frontRightLeg.Visible = false;
                // backLeftLeg.Visible = false;
                // backRightLeg.Visible = false;
                itemAnimSprites.Visible = true;
                dirtAnimSprites.Visible = true;
                SetAnimationFrame(AnimationSpriteType.Face, NONE_ANIM);
                SetAnimationFrame(AnimationSpriteType.Item, EAT_ANIM);
                SetAnimationFrame(AnimationSpriteType.Body, EAT_ANIM);
                SetAnimationFrame(AnimationSpriteType.Tail, EAT_ANIM);
                PlayAnimation(EAT_ANIM);
                break;
            case ActivityType.Clean:
                frontLeftLeg.Visible = true;
                frontRightLeg.Visible = true;
                backLeftLeg.Visible = true;
                backRightLeg.Visible = true;
                dirtAnimSprites.Visible = true;
                itemAnimSprites.Visible = true;
                SetAnimationFrame(AnimationSpriteType.Face, NEUTRAL_ANIM);
                SetAnimationFrame(AnimationSpriteType.Item, CLEAN_ANIM);
                SetAnimationFrame(AnimationSpriteType.Body, NEUTRAL_ANIM);
                SetAnimationFrame(AnimationSpriteType.Tail, NEUTRAL_ANIM);
                PlayAnimation(CLEAN_ANIM);
                break;
            case ActivityType.Medicine:
                // frontLeftLeg.Visible = false;
                // frontRightLeg.Visible = false;
                // backLeftLeg.Visible = false;
                // backRightLeg.Visible = false;
                itemAnimSprites.Visible = true;
                dirtAnimSprites.Visible = true;
                SetAnimationFrame(AnimationSpriteType.Face, NONE_ANIM);
                SetAnimationFrame(AnimationSpriteType.Item, MEDICINE_ANIM);
                SetAnimationFrame(AnimationSpriteType.Body, EAT_ANIM);
                SetAnimationFrame(AnimationSpriteType.Tail, EAT_ANIM);
                PlayAnimation(EAT_ANIM);
                break;
            default: break;
        }
    }

    public void PlayReaction(ReactionType reaction) {
        switch (reaction) {
            case ReactionType.Delighted:
                frontLeftLeg.Visible = true;
                frontRightLeg.Visible = true;
                backLeftLeg.Visible = true;
                backRightLeg.Visible = true;
                itemAnimSprites.Visible = false;
                effectAnimSprites.Visible = true;
                SetAnimationFrame(AnimationSpriteType.Face, DELIGHTED_ANIM);
                SetAnimationFrame(AnimationSpriteType.Effect, HAPPY_ANIM);
                SetAnimationFrame(AnimationSpriteType.Body, NEUTRAL_ANIM);
                SetAnimationFrame(AnimationSpriteType.Tail, NEUTRAL_ANIM);
                PlayAnimation(NEUTRAL_ANIM);
                GameCamera.OnSetMode(GameCamera.Mode.Reaction, 0f);
                break;
            case ReactionType.Neutral:
                frontLeftLeg.Visible = true;
                frontRightLeg.Visible = true;
                backLeftLeg.Visible = true;
                backRightLeg.Visible = true;
                itemAnimSprites.Visible = false;
                SetAnimationFrame(AnimationSpriteType.Face, NEUTRAL_ANIM);
                SetAnimationFrame(AnimationSpriteType.Effect, NEUTRAL_ANIM);
                SetAnimationFrame(AnimationSpriteType.Body, NEUTRAL_ANIM);
                SetAnimationFrame(AnimationSpriteType.Tail, NEUTRAL_ANIM);
                PlayAnimation(NEUTRAL_ANIM);
                GameCamera.OnSetMode(GameCamera.Mode.Reaction, 0f);
                break;
            case ReactionType.Angry:
                frontLeftLeg.Visible = true;
                frontRightLeg.Visible = true;
                backLeftLeg.Visible = true;
                backRightLeg.Visible = true;
                itemAnimSprites.Visible = false;
                SetAnimationFrame(AnimationSpriteType.Face, ANGRY_ANIM);
                SetAnimationFrame(AnimationSpriteType.Body, NEUTRAL_ANIM);
                SetAnimationFrame(AnimationSpriteType.Tail, NEUTRAL_ANIM);
                PlayAnimation(NEUTRAL_ANIM);
                GameCamera.OnSetMode(GameCamera.Mode.Reaction, 1f);
                break;
            default: break;
        }
    }

    public void PlayAnimation(string animationKey, AnimatorType animatorType = AnimatorType.Default) {
        switch (animatorType)
        {
            case AnimatorType.Effect:
                effectAnimPlayer.Play(animationKey);
                break;
            default:
                animPlayer.Play(animationKey);
                break;
        }
    }

    public string GetAnimKeyFromPrio(Stats stats, AnimationSpriteType spriteType)
    {
        switch (spriteType) {
            case AnimationSpriteType.Body:
                return GetAnimKeyFromPrio(stats, bodyAnimPrios);
            case AnimationSpriteType.Tail:
                return GetAnimKeyFromPrio(stats, tailAnimPrios);
            case AnimationSpriteType.Face:
                return GetAnimKeyFromPrio(stats, faceAnimPrios);
            case AnimationSpriteType.Dirt:
                return GetAnimKeyFromPrio(stats, dirtAnimPrios);
            default:
                return NEUTRAL_ANIM;
        }
    }

    public string GetAnimKeyFromPrio(Stats stats, EntityAnimStatePriorityValue[] prioArray)
    {
        int length = prioArray.Length - 1;
        for (int i = 0; i <= length; i++)
        {
            if (stats.GetStatus(prioArray[i].statType) == prioArray[i].status)
            {
                return prioArray[i].animKey;
            }
        }
        return prioArray[length].animKey;
    }
}

public struct EntityAnimStatePriorityValue
{
    public Stats.Type statType;
    public Status.StatusName status;
    public string animKey;

    public EntityAnimStatePriorityValue(Stats.Type statType, Status.StatusName status, string animKey)
    {
        this.statType = statType;
        this.status = status;
        this.animKey = animKey;
    }
}