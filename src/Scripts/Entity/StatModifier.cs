public struct StatModifier {
    public Stats.Type statType;
    public float multiplier;

    public StatModifier(Stats.Type statType, float multiplier) {
        this.statType = statType;
        this.multiplier = multiplier;
    }
}