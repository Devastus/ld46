using Godot;
using System;

public class EntitySound: AudioStreamPlayer2D {

    [Export]
    public Godot.Collections.Array<AudioStream> happySounds;
    [Export]
    public Godot.Collections.Array<AudioStream> sadSounds;
    [Export]
    public Godot.Collections.Array<AudioStream> delightedSounds;
    [Export]
    public Godot.Collections.Array<AudioStream> angrySounds;
    [Export]
    public Godot.Collections.Array<AudioStream> hungrySounds;
    [Export]
    public Godot.Collections.Array<AudioStream> eatSounds;
    [Export]
    public Godot.Collections.Array<AudioStream> sickSounds;
    [Export]
    public Godot.Collections.Array<AudioStream> bathroomSounds;
    [Export]
    public Godot.Collections.Array<AudioStream> cleanSounds;
    [Export]
    public Godot.Collections.Array<AudioStream> playSounds;
    [Export]
    public Godot.Collections.Array<AudioStream> fatalSounds;
    [Export]
    public Godot.Collections.Array<AudioStream> poopSounds;

    public override void _Ready()
    {

    }

    public void PlaySound(Status.StatusName type)
    {
        float pitch = (float)GD.RandRange(0.92f, 1.08f);
        AudioStream clip = GetSoundClip(type);
        if (clip == null) return;
        PlaySound(clip, pitch);
    }

    public void PlaySound(AudioStream soundClip, float pitch)
    {
        this.Stream = soundClip;
        this.PitchScale = pitch;
        this.Play();
    }

    private AudioStream GetSoundClip(Status.StatusName type)
    {
        switch (type)
        {
            case Status.StatusName.Angry:
                return GetFromSounds(angrySounds);
            case Status.StatusName.Happy:
                return GetFromSounds(happySounds);
            case Status.StatusName.Delighted:
                return GetFromSounds(delightedSounds);
            case Status.StatusName.Play:
                return GetFromSounds(playSounds);
            case Status.StatusName.Sad:
                return GetFromSounds(sadSounds);
            case Status.StatusName.Hungry:
                return GetFromSounds(hungrySounds);
            case Status.StatusName.Eat:
                return GetFromSounds(eatSounds);
            case Status.StatusName.Sick:
                return GetFromSounds(sickSounds);
            case Status.StatusName.Critical:
                return GetFromSounds(sickSounds);
            case Status.StatusName.Bathroom:
                return GetFromSounds(bathroomSounds);
            case Status.StatusName.Poop:
                return GetFromSounds(poopSounds);
            case Status.StatusName.Clean:
                return GetFromSounds(cleanSounds);
            case Status.StatusName.Fatal:
                return GetFromSounds(fatalSounds);
            default:
                break;
        }
        return null;
    }

    private AudioStream GetFromSounds(Godot.Collections.Array<AudioStream> sounds)
    {
        if (sounds.Count < 1) return null;
        int index = (int)GD.RandRange(0, sounds.Count);
        return sounds[index];
    }
}