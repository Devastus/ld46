using Godot;
using System;

public static class Status {
    public enum StatusName {
        None,
        Healthy,
        Full,
        Clean,
        Happy,
        Delighted,
        Energetic,
        Poisoned,
        Stuffed,
        Neutral,
        Sick,
        Hungry,
        Dirty,
        Messy,
        Sad,
        Tired,
        Collapsing,
        Critical,
        Fatal,
        Dump,
        Eat,
        Food,
        Bathroom,
        Poop,
        Play,
        Sleep,
        Angry
    }

    public static StatusPriority[] statusPriorities = {
        new StatusPriority(Stats.Type.Health, Status.StatusName.Critical),
        new StatusPriority(Stats.Type.Fullness, Status.StatusName.Critical),
        new StatusPriority(Stats.Type.Health, Status.StatusName.Sick),
        new StatusPriority(Stats.Type.Health, Status.StatusName.Poisoned),
        new StatusPriority(Stats.Type.Fullness, Status.StatusName.Stuffed),
        new StatusPriority(Stats.Type.Fullness, Status.StatusName.Hungry),
        new StatusPriority(Stats.Type.Energy, Status.StatusName.Tired),
        new StatusPriority(Stats.Type.Happiness, Status.StatusName.Sad),
        new StatusPriority(Stats.Type.Cleanliness, Status.StatusName.Dirty),
        new StatusPriority(Stats.Type.Cleanliness, Status.StatusName.Messy),
        new StatusPriority(Stats.Type.Happiness, Status.StatusName.Neutral),
        new StatusPriority(Stats.Type.Happiness, Status.StatusName.Happy)
    };

    public static StatusName GetStatusByPriority(Stats stats) {
        for (int i = 0; i < statusPriorities.Length; i++) {
            if (stats.GetStatus(statusPriorities[i].statType) == statusPriorities[i].status) {
                return statusPriorities[i].status;
            }
        }
        return statusPriorities[statusPriorities.Length-1].status;
    }

    public class StatusPriority {
        public Stats.Type statType;
        public StatusName status;

        public StatusPriority(Stats.Type statType, StatusName status)
        {
            this.statType = statType;
            this.status = status;
        }
    }

    public class StatusMap {
        private StatusValue[] statusValues;

        public StatusMap(params StatusValue[] statusValues) {
            this.statusValues = statusValues;
        }

        public StatusName GetStatus(int value) {
            if (value <= statusValues[0].value) return statusValues[0].name; 
            int length = statusValues.Length-1;

            for (int i = 0; i < length; i++) {
                if (value >= statusValues[i].value && value <= statusValues[i+1].value) {
                    return statusValues[i].name;
                }
            }

            if (value >= statusValues[length].value) return statusValues[length].name; 
            return Status.StatusName.None;
        }
    }

    public struct StatusValue {
        public Status.StatusName name;
        public int value;

        public StatusValue(Status.StatusName name, int value) {
            this.name = name;
            this.value = value;
        }
    }

    public struct StatusTransition {
        public Status.StatusName from;
        public Status.StatusName to;

        public StatusTransition(Status.StatusName from, Status.StatusName to) {
            this.from = from;
            this.to = to;
        }

        public override bool Equals(object obj) {
            if (obj == null) return false;
            if (!(obj is StatusTransition)) return false;
            StatusTransition other = (StatusTransition)obj;
            return from == other.from && to == other.to;
        }

        public override int GetHashCode() {
            return from.GetHashCode() ^ to.GetHashCode();
        }
    }
}