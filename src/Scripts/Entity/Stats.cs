using Godot;
using System;
using System.Collections.Generic;
using static Status;

public class Stats
{
    public System.Action<StatusName> OnStatusUpdate;

    public enum Type
    {
        Health,
        Fullness,
        Happiness,
        Cleanliness,
        Energy,
        Bathroom
    }

    private StatValue[] _stats;
    private StatusName[] _statuses;
    private Dictionary<StatusName, List<StatModifier>> _modifiers;
    private StatusMap[] _statusMaps;

    public Stats(params StatValue[] stats) {
        _stats = stats;
    }

    public Stats(StatValue[] statValues, StatusMap[] statusMaps, StatusName[] statuses, Dictionary<StatusName, List<StatModifier>> statModifiers) {
        this._stats = statValues;
        this._modifiers = statModifiers;
        this._statusMaps = statusMaps;
        this._statuses = statuses;
    }

    public StatValue this[Type type]
    {
        get { return _stats[(int)type]; }
    }

    public StatValue this[int index]
    {
        get { return _stats[index]; }
    }

    public void Update()
    {
        for (int i = 0; i < _stats.Length; i++) {
            _stats[i].Update();
            var curStatus = _statusMaps[i].GetStatus(_stats[i].value);
            if (_statuses[i] != curStatus) {
                RemoveModifiers(GetStatusModifiers(_statuses[i]));
                AddModifiers(GetStatusModifiers(curStatus));
                _statuses[i] = curStatus;
                if (OnStatusUpdate != null) OnStatusUpdate(curStatus);
            }
        }
    }

    public void SetRate(Type type, int value) {
        _stats[(int)type].SetRate(value);
    }

    public void SetUpdateMode(Type type, StatValue.UpdateMode mode) {
        _stats[(int)type].SetUpdateMode(mode);
    }

    public void Sum(Type type, int value)
    {
        _stats[(int)type].Sum(value);
    }

    public void Set(Type type, int value)
    {
        _stats[(int)type].Set(value);
    }

    public int GetValue(Type type) {
        return _stats[(int)type].value;
    }

    public StatusName GetStatus(Type type) 
    {
        int typeIndex = (int)type;
        int value = _stats[typeIndex].value;
        return _statusMaps[typeIndex].GetStatus(value);
    }

    public List<StatModifier> GetStatusModifiers(StatusName status) {
        List<StatModifier> mods = null;
        if (_modifiers.TryGetValue(status, out mods)) {
            return mods;
        }
        return null;
    }

    public void AddModifiers(List<StatModifier> modifiers) {
        if (modifiers != null) {
            foreach(var mod in modifiers) {
                ModifyStat(mod.statType, mod.multiplier);
            }
        }
    }

    public void RemoveModifiers(List<StatModifier> modifiers) {
        if (modifiers != null) {
            foreach(var mod in modifiers) {
                ModifyStat(mod.statType, -mod.multiplier);
            }
        }
    }

    public void ModifyStat(Stats.Type statType, float multiplier) {
        _stats[(int)statType].multiplier += multiplier;
    }
}