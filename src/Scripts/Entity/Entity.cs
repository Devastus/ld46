using Godot;
using System;
using System.Collections.Generic;
using Utils;
using static Status;

public class Entity : Node2D
{

    public enum InteractionState
    {
        Idle,
        Active,
        Reaction,
        Sleeping,
        Dead
    }

    [Export(PropertyHint.Range, "0,10")]
    private float minMoveWaitTime = 2f;
    [Export(PropertyHint.Range, "0,10")]
    private float maxMoveWaitTime = 4f;
    [Export(PropertyHint.Range, "0,100")]
    private float happyMoveSpeed = 50f;
    [Export(PropertyHint.Range, "0,100")]
    private float neutralMoveSpeed = 40f;
    [Export(PropertyHint.Range, "0,100")]
    private float sickMoveSpeed = 30f;

    [Export(PropertyHint.Range, "1,100")]
    public int healthRate = 5;
    [Export(PropertyHint.Range, "1,100")]
    public int fullnesRate = 2;
    [Export(PropertyHint.Range, "1,100")]
    public int happinessRate = 3;
    [Export(PropertyHint.Range, "1,100")]
    public int cleanlinessRate = 3;
    [Export(PropertyHint.Range, "1,100")]
    public int energyRate = 3;
    [Export(PropertyHint.Range, "1,100")]
    public int bathroomRate = 5;
    [Export(PropertyHint.Range, "1,100")]
    public int poopEffectOnCleanliness = 2;
    [Export(PropertyHint.Range, "1,100")]
    public int minStatusSoundTime = 7;
    [Export(PropertyHint.Range, "1,100")]
    public int maxStatusSoundTime = 14;

    private InteractionState _interactionState = InteractionState.Idle;
    private Stats _stats;
    private EntityAnimator _anim = new EntityAnimator();
    private EntityAnimator.ReactionType _activityReaction;
    private ReactionContainer _storedReaction;

    private EntitySound _sound;
    private StatusName _curHighestPriorityStatus = StatusName.Neutral;
    private GameArea _curGameArea;

    public InteractionState CurInteractionState { get { return _interactionState; } }
    public Stats CurStats { get { return _stats; } }

    private float _actionTimer = 0f;
    private Vector2 _nextPos;
    private Vector2 _dir;
    private Vector2 _origScale;
    private bool _moving = false;
    private float _curMoveSpeed;
    private int _nextStatusSound = 10;

    private List<Node2D> poops = new List<Node2D>();

    // public override void _Draw()
    // {
    //     // if (GameManager.Instance.Debug)
    //     // {
    //         // Vector2 pos = Position - _nextPos;
    //         // DrawCircle(pos, 10f, new Color(1f, 0f, 0f, 0.8f));
    //     // }
    // }

    public override void _Ready()
    {
        _interactionState = InteractionState.Idle;
        _stats = StatConfig.InitStats(this);
        _stats.OnStatusUpdate += ReactToStatusChange;
        _anim.Init(this);
        _sound = (EntitySound)GetNode("EntitySound");
        

        TickManager.OnTickUpdate += UpdateEntity;
        UIManager.OnPressActivity += EvaluateActivity;

        _nextPos = Position;
        _dir = new Vector2(0, 0);
        _origScale = Scale;
        _moving = false;
        _curMoveSpeed = neutralMoveSpeed;

        SetInteractionState(InteractionState.Idle, maxMoveWaitTime);
        _stats.Update();
        _curGameArea = (GameArea)GetParent().GetNode("GameArea");
        GameCamera.OnSetTarget(this);
    }

    public override void _ExitTree()
    {
        TickManager.OnTickUpdate -= UpdateEntity;
        UIManager.OnPressActivity -= EvaluateActivity;
    }

    public override void _Process(float delta)
    {
        if (_interactionState != InteractionState.Dead)
        {
            ZIndex = (int)Position.y;

            switch (_interactionState)
            {
                // ACTIVE ACTION
                case InteractionState.Active:
                    _actionTimer -= delta;
                    if (_actionTimer <= 0f)
                    {
                        SetInteractionState(InteractionState.Reaction, 1f);
                        PlayStoredReaction();
                    }
                    break;
                case InteractionState.Reaction:
                    _actionTimer -= delta;
                    if (_actionTimer <= 0f)
                    {
                        SetInteractionState(InteractionState.Idle);
                        UpdateStatus();
                        ResetMove();
                    }
                    break;
                // SLEEPING
                case InteractionState.Sleeping:
                    break;
                // IDLE
                default:
                    if (_moving)
                    {
                        Move(delta);
                    }
                    else
                    {
                        Wait(delta);
                    }
                    break;
            }
        }

        if (GameManager.Instance.Debug)
        {
            if (StatusDebugger.OnDebugStatus != null) StatusDebugger.OnDebugStatus(_stats);
            Update();
        }
    }

    private void Move(float delta)
    {
        if (!_curGameArea.HasPosition(Position))
        {
            Position = _curGameArea.ClampPositionToRect(Position, new Vector2(3f, 3f));
            ResetMove();
            return;
        }

        Vector2 diff = _nextPos - Position;
        float distance = (
            diff.x * diff.x +
            diff.y * diff.y
        );
        if (distance > 0.5f)
        {
            Position += _dir * happyMoveSpeed * delta;
        }
        else
        {
            ResetMove();
        }
    }

    private void Wait(float delta)
    {
        _actionTimer -= delta;
        if (_actionTimer <= 0f)
        {
            _actionTimer = 0f;
            GetRandomPosition();
            _anim.PlayAnimation(EntityAnimator.WALK_ANIM);
            _moving = true;
        }
    }

    private void ResetMove()
    {
        _moving = false;
        _dir = new Vector2(0f, 0f);
        _nextPos = Position;
        _actionTimer = (float)GD.RandRange(minMoveWaitTime, maxMoveWaitTime);
        _anim.PlayAnimation(EntityAnimator.NEUTRAL_ANIM);
    }

    private void GetRandomPosition()
    {
        Vector2 margin = new Vector2(3f, 3f);
        Rect2 areaRect = _curGameArea.GetRect(margin);
        float x = (float)GD.RandRange(areaRect.Position.x, areaRect.Position.x + areaRect.Size.x);
        float y = (float)GD.RandRange(areaRect.Position.y, areaRect.Position.y + areaRect.Size.y);
        _nextPos = _curGameArea.ClampPositionToRect(new Vector2(x, y), margin);

        _dir = (_nextPos - Position).Normalized();
        if (Mathf.Sign(_dir.x) > 0f)
        {
            Scale = new Vector2(_origScale.x * -1, _origScale.y);
        }
        else
        {
            Scale = _origScale;
        }
    }

    private void UpdateEntity()
    {
        if (_interactionState == InteractionState.Idle ||
            _interactionState == InteractionState.Sleeping
        )
        {
            _stats.Update();
            UpdateStatus();
        }
    }

    /// Evaluate reaction to a Player activity
    public void EvaluateActivity(PlayerActivity activity)
    {
        if (activity == PlayerActivity.Status) return;

        if (_interactionState == InteractionState.Idle)
        {
            switch (activity)
            {
                case PlayerActivity.Food:
                    SetInteractionState(InteractionState.Active);
                    PlayActivityWithReaction(EntityAnimator.ActivityType.Eat, new ReactionContainer(
                        EntityAnimator.ReactionType.Delighted,
                        new ReactionStat(Stats.Type.Fullness, 25),
                        new ReactionStat(Stats.Type.Bathroom, 20),
                        new ReactionStat(Stats.Type.Cleanliness, -5)
                    ));
                    _sound.PlaySound(Status.StatusName.Eat);
                    break;
                case PlayerActivity.Play:
                    SetInteractionState(InteractionState.Active);
                    PlayActivityWithReaction(EntityAnimator.ActivityType.Play, new ReactionContainer(
                        EntityAnimator.ReactionType.Delighted,
                        new ReactionStat(Stats.Type.Happiness, 25),
                        new ReactionStat(Stats.Type.Energy, -15),
                        new ReactionStat(Stats.Type.Fullness, -5)
                    ));
                    _sound.PlaySound(Status.StatusName.Play);
                    break;
                case PlayerActivity.Medicine:
                    SetInteractionState(InteractionState.Active);
                    PlayActivityWithReaction(EntityAnimator.ActivityType.Medicine, new ReactionContainer(
                        EntityAnimator.ReactionType.Angry,
                        new ReactionStat(Stats.Type.Health, 25),
                        new ReactionStat(Stats.Type.Happiness, -15)
                    ));
                    _sound.PlaySound(Status.StatusName.Eat);
                    break;
                case PlayerActivity.Clean:
                    SetInteractionState(InteractionState.Active);
                    if (_stats.GetStatus(Stats.Type.Cleanliness) == Status.StatusName.Clean)
                    {
                        PlayActivityWithReaction(EntityAnimator.ActivityType.Clean, new ReactionContainer(
                            EntityAnimator.ReactionType.Angry,
                            new ReactionStat(Stats.Type.Cleanliness, 25),
                            new ReactionStat(Stats.Type.Energy, -10),
                            new ReactionStat(Stats.Type.Happiness, -10)
                        ));
                    }
                    else
                    {
                        PlayActivityWithReaction(EntityAnimator.ActivityType.Clean, new ReactionContainer(
                            EntityAnimator.ReactionType.Neutral,
                            new ReactionStat(Stats.Type.Cleanliness, 25),
                            new ReactionStat(Stats.Type.Energy, -10)
                        ));
                    }
                    _sound.PlaySound(Status.StatusName.Clean);
                    break;
                case PlayerActivity.Bathroom:
                    _sound.PlaySound(Status.StatusName.Bathroom);
                    for (int i = 0; i < poops.Count; i++) {
                        GameManager.Instance.DespawnNode(poops[i]);
                        poops.RemoveAt(i);
                    }
                    break;
                case PlayerActivity.Sleep:
                    _sound.PlaySound(Status.StatusName.Sleep);
                    if (_stats.GetStatus(Stats.Type.Energy) == Status.StatusName.Energetic)
                    {
                        GiveStat(Stats.Type.Happiness, -10);
                        SetInteractionState(InteractionState.Reaction, 1f, true);
                        _anim.PlayReaction(EntityAnimator.ReactionType.Angry);
                        _sound.PlaySound(Status.StatusName.Angry);
                    }
                    else
                    {
                        SetInteractionState(InteractionState.Sleeping);
                    }
                    break;
                case PlayerActivity.ClickOn:
                    // GD.Print("MIAU");
                    break;
                default: break;
            }
            _stats.Update();
        }
    }

    public void PlayActivityWithReaction(EntityAnimator.ActivityType activityType, ReactionContainer storedReaction)
    {
        _storedReaction = storedReaction;
        _anim.PlayActivity(activityType);
    }

    public void PlayStoredReaction()
    {
        if (_storedReaction != null)
        {
            _anim.PlayReaction(_storedReaction.reactionType);

            if (_storedReaction.reactionType == EntityAnimator.ReactionType.Angry) {
                _sound.PlaySound(Status.StatusName.Angry);
            }
            else if (_storedReaction.reactionType == EntityAnimator.ReactionType.Delighted) {
                _sound.PlaySound(Status.StatusName.Delighted);
            }

            foreach (var stat in _storedReaction.reactionStats)
            {
                _stats.Sum(stat.statType, stat.value);
            }
            _storedReaction = null;
        }
    }

    public void GiveStat(Stats.Type statType, int value)
    {
        _stats.Sum(statType, value);
        UpdateStatus();
    }

    public void SetInteractionState(InteractionState state, float actionTimer = 2f, bool wiggle = false)
    {
        _interactionState = state;
        _actionTimer = actionTimer;
        SetSpriteVisibility(state);
        switch (state)
        {
            case InteractionState.Sleeping:
                _anim.PlayAnimation(EntityAnimator.SLEEP_ANIM);
                _anim.SetAnimationFrame(EntityAnimator.AnimationSpriteType.Effect, EntityAnimator.SLEEP_ANIM);
                _stats.SetUpdateMode(Stats.Type.Energy, StatValue.UpdateMode.Additive);
                SetSleepModifiers(false);
                GameCamera.OnSetMode(GameCamera.Mode.Activity, 0f);
                _moving = false;
                break;
            case InteractionState.Dead:
                _anim.SetAnimationFrame(EntityAnimator.AnimationSpriteType.Effect, EntityAnimator.DEAD_ANIM);
                _anim.PlayAnimation(EntityAnimator.NONE_ANIM);
                _sound.PlaySound(StatusName.Fatal);
                GameCamera.OnSetMode(GameCamera.Mode.Default, 0f);
                UIManager.OnSetScreen(UIManager.Screen.End);
                _moving = false;
                break;
            case InteractionState.Active:
                GameCamera.OnSetMode(GameCamera.Mode.Activity, 0f);
                _moving = false;
                break;
            case InteractionState.Reaction:
                _moving = false;
                break;
            default:
                _anim.SetAnimationFrame(EntityAnimator.AnimationSpriteType.Effect, EntityAnimator.NEUTRAL_ANIM);
                _stats.SetUpdateMode(Stats.Type.Energy, StatValue.UpdateMode.Subtractive);
                GameCamera.OnSetMode(GameCamera.Mode.Default, 0f);
                break;
        }
    }

    public void SetSleepModifiers(bool remove)
    {
        List<StatModifier> modifiers = new List<StatModifier>(){
            new StatModifier(Stats.Type.Health, -0.35f),
            new StatModifier(Stats.Type.Fullness, -0.35f),
            new StatModifier(Stats.Type.Happiness, -0.35f)
        };
        if (remove)
        {
            _stats.RemoveModifiers(modifiers);
        }
        else
        {
            _stats.AddModifiers(modifiers);
        }
    }

    public void SetSpriteVisibility(InteractionState interactionState)
    {
        switch (interactionState)
        {
            case InteractionState.Sleeping:
                _anim.SetSpriteVisibility(EntityAnimator.AnimationType.Sleeping);
                break;
            case InteractionState.Dead:
                _anim.SetSpriteVisibility(EntityAnimator.AnimationType.Dead);
                break;
            default:
                _anim.SetSpriteVisibility(EntityAnimator.AnimationType.Idle);
                break;
        }
    }

    private void UpdateStatus()
    {
        // FATAL
        if (_stats.GetStatus(Stats.Type.Health) == Status.StatusName.Fatal || _stats.GetStatus(Stats.Type.Fullness) == Status.StatusName.Fatal)
        {
            SetInteractionState(InteractionState.Dead, 0f);
            return;
        }

        if (_interactionState == InteractionState.Sleeping)
        {
            _stats.Sum(Stats.Type.Energy, 3);
            if (_stats[Stats.Type.Energy].IsMax())
            {
                SetSleepModifiers(true);
                SetInteractionState(InteractionState.Reaction, 1f);
                _anim.PlayReaction(EntityAnimator.ReactionType.Delighted);
                _sound.PlaySound(Status.StatusName.Happy);
            }
        }
        else
        {
            // Force sleep
            if (_stats.GetStatus(Stats.Type.Energy) == Status.StatusName.Collapsing)
            {
                SetInteractionState(InteractionState.Sleeping, 0f);
            }

            _anim.SetAnimationFrameByPrio(_stats, EntityAnimator.AnimationSpriteType.Face);
            _anim.SetAnimationFrameByPrio(_stats, EntityAnimator.AnimationSpriteType.Tail);
            _anim.SetAnimationFrameByPrio(_stats, EntityAnimator.AnimationSpriteType.Body);
            _anim.SetAnimationFrameByPrio(_stats, EntityAnimator.AnimationSpriteType.Dirt);

            // Movement speed
            if (_stats.GetStatus(Stats.Type.Health) != Status.StatusName.Healthy ||
                _stats.GetStatus(Stats.Type.Energy) != Status.StatusName.Energetic
            )
            {
                _curMoveSpeed = sickMoveSpeed;
            }
            else if (_stats.GetStatus(Stats.Type.Happiness) == Status.StatusName.Happy)
            {
                _curMoveSpeed = happyMoveSpeed;
            }
            else
            {
                _curMoveSpeed = neutralMoveSpeed;
            }

            // Take a dump
            if (_stats.GetStatus(Stats.Type.Bathroom) == Status.StatusName.Dump)
            {
                // TODO: TAKE A DUMP
                Node2D poop = GameManager.Instance.SpawnNode<Node2D>("Poop", GameManager.Instance.CurScene);
                poop.Position = this.Position + new Vector2(-100f * (-Mathf.Sign(this.Scale.x)), 0);
                poop.ZIndex = (int)poop.Position.y;
                poops.Add(poop);
                _sound.PlaySound(StatusName.Poop);
                _stats.Set(Stats.Type.Bathroom, 0);
            }

            for (int i = 0; i < poops.Count; i++) {
                _stats.Sum(Stats.Type.Cleanliness, -poopEffectOnCleanliness);
            }
        }

        _nextStatusSound -= 1;
        if (_nextStatusSound <= 0)
        {
            _nextStatusSound = (int)GD.RandRange(minStatusSoundTime, maxStatusSoundTime);
            if (_interactionState != InteractionState.Sleeping) {
                _sound.PlaySound(_curHighestPriorityStatus);
            }
        }

        if (_interactionState != InteractionState.Dead) GameManager.Instance.pointsManager.UpdatePoints(this);
        if (UIManager.OnEntityStatsUpdate != null) UIManager.OnEntityStatsUpdate(_stats);
    }

    private void ReactToStatusChange(Status.StatusName newStatus) {
        _curHighestPriorityStatus = Status.GetStatusByPriority(_stats);
        _nextStatusSound = (int)GD.RandRange(minStatusSoundTime, maxStatusSoundTime);
        if (_interactionState != InteractionState.Sleeping) {
            _sound.PlaySound(_curHighestPriorityStatus);
        }
    }
}


public class ReactionContainer
{
    public EntityAnimator.ReactionType reactionType;
    public ReactionStat[] reactionStats;

    public ReactionContainer(EntityAnimator.ReactionType reactionType, params ReactionStat[] reactionStats)
    {
        this.reactionType = reactionType;
        this.reactionStats = reactionStats;
    }
}

public struct ReactionStat
{
    public Stats.Type statType;
    public int value;

    public ReactionStat(Stats.Type statType, int value)
    {
        this.statType = statType;
        this.value = value;
    }
}