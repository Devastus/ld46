using Godot;
using System;

public class StatusDebugger : Control
{
    public static System.Action<Stats> OnDebugStatus;
    
    private Stats _stats = null;

    [Export]
    private Font debuggerFont;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        OnDebugStatus = DebugStats;   
        GameManager.OnUpdateDebug += () => Update();
    }

    public override void _Draw() {
        if (GameManager.Instance.Debug && _stats != null) {
            DrawRect(new Rect2(new Vector2(0, 0), new Vector2(225, 100)), new Color(0f, 0f, 0f, 0.6f));

            Rect2 barRect = new Rect2(new Vector2(5, 5), new Vector2(0, 5));
            barRect = DrawStatusBar(barRect, GetStatValue(_stats, Stats.Type.Health), new Color(1f, 0f, 0f, 1f));
            barRect = DrawStatusBar(barRect, GetStatValue(_stats, Stats.Type.Fullness), new Color(0.8f, 0.8f, 0f, 1f));
            barRect = DrawStatusBar(barRect, GetStatValue(_stats, Stats.Type.Happiness), new Color(0f, 1f, 0f, 1f));
            barRect = DrawStatusBar(barRect, GetStatValue(_stats, Stats.Type.Cleanliness), new Color(0.5f, 0.8f, 3f, 1f));
            barRect = DrawStatusBar(barRect, GetStatValue(_stats, Stats.Type.Energy), new Color(0.3f, 0.3f, 1f, 1f));
            barRect = DrawStatusBar(barRect, GetStatValue(_stats, Stats.Type.Bathroom), new Color(0.8f, 0.41f, 0.18f, 1f));

            barRect.Position = new Vector2(barRect.Position.x, barRect.Position.y + 10);
            barRect.Size = new Vector2(200, 5);

            barRect = DrawStatusText(barRect, new string[]{_stats.GetStatus(Stats.Type.Health).ToString(), _stats.GetStatus(Stats.Type.Fullness).ToString()});
            barRect = DrawStatusText(barRect, new string[]{_stats.GetStatus(Stats.Type.Happiness).ToString(), _stats.GetStatus(Stats.Type.Cleanliness).ToString()});
            barRect = DrawStatusText(barRect, new string[]{_stats.GetStatus(Stats.Type.Energy).ToString(), _stats.GetStatus(Stats.Type.Bathroom).ToString()});
        }
    }
    
    public Rect2 DrawStatusBar(Rect2 rect, int value, Color color) {
        rect.Size = new Vector2(value, 5);
        DrawRect(rect, color);
        rect = new Rect2(new Vector2(rect.Position.x, rect.Position.y + 10), rect.Size);
        return rect;
    }

    public Rect2 DrawStatusText(Rect2 rect, string[] values, Color? color = null) {
        float origXPos = rect.Position.x;
        float segment = rect.Size.x / (float)values.Length;
        for (int i = 0; i < values.Length; i++) {
            float xPos = origXPos  + (segment * i);
            rect.Position = new Vector2(xPos, rect.Position.y);
            DrawString(debuggerFont, rect.Position, values[i], color);
        }
        rect = new Rect2(new Vector2(origXPos, rect.Position.y + 10), rect.Size);
        return rect;
    }

    private int GetStatValue(Stats stats, Stats.Type type) {
        StatValue statValue = stats[type];
        if (statValue.type == StatValue.StatType.Linear) {
            return statValue.value * 2;
        } else {
            return statValue.value;
        }
    }

    public void DebugStats(Stats stats) {
        _stats = stats;
        Update();
    }
}
