using System.Collections.Generic;
using static Status;

public static class StatConfig {
    public static Stats InitStats(Entity entity) {
        // Stat values
        var statValues = new StatValue[]{
            new StatValue(StatValue.StatType.Balance, 100, entity.healthRate, StatValue.UpdateMode.Subtractive),
            new StatValue(StatValue.StatType.Balance, 100, entity.fullnesRate, StatValue.UpdateMode.Subtractive),
            new StatValue(StatValue.StatType.Linear, 59, entity.happinessRate, StatValue.UpdateMode.Subtractive),
            new StatValue(StatValue.StatType.Linear, 100, entity.cleanlinessRate, StatValue.UpdateMode.Subtractive),
            new StatValue(StatValue.StatType.Linear, 100, entity.energyRate, StatValue.UpdateMode.Subtractive),
            new StatValue(StatValue.StatType.Linear, 0, entity.bathroomRate, StatValue.UpdateMode.Additive)
        };

        // Map stat values to statuses
        var statusMaps = new StatusMap[]{
            // HEALTH
            new StatusMap(
                new StatusValue(Status.StatusName.Fatal, 0),
                new StatusValue(Status.StatusName.Critical, 1),
                new StatusValue(Status.StatusName.Sick, 30),
                new StatusValue(Status.StatusName.Healthy, 70),
                new StatusValue(Status.StatusName.Poisoned, 130),
                new StatusValue(Status.StatusName.Critical, 170),
                new StatusValue(Status.StatusName.Fatal, 199)
            ),
            // FULLNESS
            new StatusMap(
                new StatusValue(Status.StatusName.Fatal, 0),
                new StatusValue(Status.StatusName.Critical, 1),
                new StatusValue(Status.StatusName.Hungry, 30),
                new StatusValue(Status.StatusName.Full, 80),
                new StatusValue(Status.StatusName.Stuffed, 120),
                new StatusValue(Status.StatusName.Critical, 170),
                new StatusValue(Status.StatusName.Fatal, 199)
            ),
            // HAPPINESS
            new StatusMap(
                new StatusValue(Status.StatusName.Sad, 0),
                new StatusValue(Status.StatusName.Neutral, 30),
                new StatusValue(Status.StatusName.Happy, 60)
            ),
            // CLEANLINESS
            new StatusMap(
                new StatusValue(Status.StatusName.Dirty, 0),
                new StatusValue(Status.StatusName.Messy, 20),
                new StatusValue(Status.StatusName.Clean, 50)
            ),
            // ENERGY
            new StatusMap(
                new StatusValue(Status.StatusName.Collapsing, 0),
                new StatusValue(Status.StatusName.Tired, 1),
                new StatusValue(Status.StatusName.Energetic, 50)
            ),
            new StatusMap(
                new StatusValue(Status.StatusName.Neutral, 0),
                new StatusValue(Status.StatusName.Dump, 99)
            )
        };

        // Init Status cache
        var statuses = new Status.StatusName[]{
            Status.StatusName.Healthy,
            Status.StatusName.Full,
            Status.StatusName.Neutral,
            Status.StatusName.Clean,
            Status.StatusName.Energetic,
            Status.StatusName.Neutral
        };

        // Initialize modifiers
        var statModifiers = new Dictionary<Status.StatusName, List<StatModifier>>();
        statModifiers[Status.StatusName.Hungry] = new List<StatModifier>{new StatModifier(Stats.Type.Health, 2f)};
        statModifiers[Status.StatusName.Messy] = new List<StatModifier>{new StatModifier(Stats.Type.Health, 2f)};
        statModifiers[Status.StatusName.Dirty] = new List<StatModifier>{new StatModifier(Stats.Type.Health, 4f)};
        statModifiers[Status.StatusName.Stuffed] = new List<StatModifier>{new StatModifier(Stats.Type.Health, 1f)};
        statModifiers[Status.StatusName.Poisoned] = new List<StatModifier>{new StatModifier(Stats.Type.Happiness, 1.5f)};
        statModifiers[Status.StatusName.Critical] = new List<StatModifier>{new StatModifier(Stats.Type.Health, 4f)};
        // statModifiers[Status.StatusName.Neutral] = new List<StatModifier>{new StatModifier(Stats.Type.Cleanliness, 0.5f)};
        statModifiers[Status.StatusName.Sad] = new List<StatModifier>{new StatModifier(Stats.Type.Cleanliness, 1f), new StatModifier(Stats.Type.Bathroom, 3f)};

        // Initialize stats
        Stats stats = new Stats(
            statValues,
            statusMaps,
            statuses,
            statModifiers
        );

        return stats;
    }
}