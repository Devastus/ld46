using Utils;

public struct StatValue
{
    public enum StatType
    {
        Linear,
        Balance
    }

    public enum UpdateMode
    {
        Additive,
        Subtractive
    }

    public int value;
    public int rate;
    public float multiplier;
    public StatType type;
    public UpdateMode updateMode;

    private int _maxValue;
    private int _minValue;
    private int _curRateTick;

    public StatValue(StatType type, int value, int rate, UpdateMode updateMode = UpdateMode.Subtractive, float multiplier = 1f)
    {
        if (type == StatType.Linear)
        {
            _minValue = 0;
            _maxValue = 100;
        }
        else
        {
            _minValue = 0;
            _maxValue = 200;
        }

        this.value = MathUtils.Clamp(value, _minValue, _maxValue);
        this.rate = rate;
        this.type = type;
        this.multiplier = multiplier;
        this.updateMode = updateMode;
        this._curRateTick = 0;
    }

    public void Update()
    {
        int realRate = (int)System.Math.Max((rate / multiplier), 1);
        if (_curRateTick >= realRate)
        {
            int sumValue = (int)System.Math.Max((1 * multiplier), 1);
            if (updateMode == UpdateMode.Additive)
            {
                Sum(sumValue);
            }
            else
            {
                Sum(-sumValue);
            }
        }
        _curRateTick++;
    }

    public void SetRate(int tickRate)
    {
        rate = tickRate;
    }

    public void SetUpdateMode(UpdateMode mode) {
        this.updateMode = mode;
    }

    public void Set(int value)
    {
        this.value = MathUtils.Clamp(value, _minValue, _maxValue);
        _curRateTick = 0;
    }

    public void Sum(int value)
    {
        this.value = MathUtils.Clamp(this.value + value, _minValue, _maxValue);
        _curRateTick = 0;
    }

    public bool IsMin() { return value == _minValue; }
    public bool IsMax() { return value == _maxValue; }
    public bool IsAtLimit()
    {
        if (type == StatType.Balance)
        {
            return value == _minValue || value == _maxValue;
        }
        else
        {
            return value == _minValue;
        }
    }
}